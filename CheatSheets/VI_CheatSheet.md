# VI Cheat Sheet
Vi is a very powerful text editor found on virtually any Unix or Linux system.
Irrispective of any debate about the 'best' editor available it's nice to
know the minimum about it so that you can get around.

As a quick overview VI has two modes "Command mode" (where you can issue the
below commands) and "Edit mode" where you can type normal text.
These are often referred to as "Beep mode" and "mangle mode."


# Navigation commands
* Basic navigation is done with `j`,`k`,`l`,`;` or arrow keys.
* `^` (shift 6) = Beginning of line
* `$` = End of line
* `G` = Last line in document
* `1G` (the number one then shift+g) = The first line in the document. `1` can be replaced with any line number
* `w` = Move ahead one word
* `b` = Move back one word

# Editing commands
* `d` = Delete
* `c` = Change
* `y` = Yank (copy)
* `p` = Paste
* `i` = Go in to edit mode to the left of the cursor
* `a` = Go into edit mode to the right of the cursor
* `ESC` = Exit mangle (edit) mode and return to beep (command) mode

## One of the things that makes vi powerful is that editing commands can be combined with any of the navigation commands.
* d(range) = Delete
  * `dw` = Delete the word under your cursor
  * `d3w` = Delete three words
  * `dd` = Delete the line under your cursor
  * `d2d` = Delete two lines.
* c(range) = Change
* `cw` = Change the word under your cursor
* `c$` = Dhange all words until the end of the line.
* `yG` = Yank (copy) all lines from here until the end of the document
* `.` = Perform the last command executed again

# Searching
* `/<somestring>` = Search forward in a document
* `?<somestring>` = Search backwards in a document
* `n` = The next result in your search

# Saving/Reloading/exiting
* `:q` = Quit
* `:w` = Save (write)

## these can be combined
* `:wq` = Save and quit
* `!` Can be appended to force
* `:e!` = Reload the file from disk. Very handy if you've accidentally done something that has potentially made changes that you don't want to save.

# Examples
## Change multiple occurances of one pattern to another
1) `/oldtext<enter>` (your cursor is now moved to the first occurance of oldtext)
1) `cwnewtext<ESC>` (oldtext is gone and now replaced with newtext)
1) `n` (your cursor is now moved to the next occurance of oldtext)
1) `.` (oldtext is gone and now replaced with newtext)
1) (repeat as needed)


# Notes
## Many lowercase VI commands have an uppercase equivalent that functions similarly
for example
* `a` = Go into edit mode after where the cursor is. `A` = Go into edit mode at the end of the line.
* `w` = Move ahead one word. `W` = Move ahead one word but consider special characters as word characters
* `b` = Move back one word. `B` = Move back one word but consider special characters as word characters

## There is a much more powerful find and replace functionality beyond the scope of a cheat sheet.

## Once you're ready to move on
My favorite commands lists [are found here](http://www.lagmonster.org/docs/vi.html)
There is an advanced commands link near the top of that page.

## Do not use ZZ to save and quit.
It will work but can potentially mess you up because in unix CTRL+z will
put a process in the background. If you accidentally hit ctrl+z instead
of shift+z vi will dissapear and it will look like your save & quit has
worked but in reality the save has not occured because VI is asleep in
the background now.
