# Intro to Linux
A short very high level dive into the history of Linux.
we want to dive into useful stuff as quickly as possible.

  * The Unix Architecture or "Ye ol' circles diagram"
  * What is a shell
  * Workstation set up

## The Unix (and by Extension Linux) Architecture

### Review
To review, or for those who skipped [the History chapter](History.md), to
summarize. Linux originated as a ground up rewrite of Unix intended to
run on cheaper, easily available PC hardware and with a licensing model
which encouraged users to peek under the covers and get their hands dirty.

### What is a Kernel
Before we dive into practical examples let's introduce some topics and some
terminology.

First let's define an Operating System or "OS." An OS is little more than
an interface which allows humans to interact with tools which run on computer
hardware. With that in mind UNIX was designed with the following in mind.

![Ye Ol' Circles Diagram](../resources/images/yeOlCirclesDiagram.png)

#### Hardware
At it's core is obviously the hardware. Without something to run on there
is no point to an OS.

#### Kernel
The next layer up is the Kernel. You may have heard this term in the past
and wondered about specifics. Essentially the Kernel is the heart of the OS.
The kernel is the portion of the OS which interacts with the hardware. When
you want to display an image to your monitor it is your kernel which understands
how to send the appropriate signals to the monitor which will light up the
correct pixels. In the early days support for specific hardware, say
an Intel Core i7-1150G7 processor vs an AMD Ryzen 9 5950x processor, was
hard coded into the kernel. These days "drivers" for specific hardware
become part of the kernel through a process called loading kernel modules.

So now the OS can talk to the hardware. Which is great but of little use
if the OS has no tasks to send to the hardware.

#### Shell
The next layer is the shell. This was the primary way that humans interacted
with the OS. That includes both interactive, immediate actions and
automated activities (such as system start up activities.) "The shell" in
a typical UNIX/Linux OS is text based. The "Shell" in Windows (since 95)
would be the GUI you see when you first log in to your computer.

The Shell is essentially how a human interacts with the hardware of a
computer. It is also how a human interacts with the various applications
which do the things said human actually cares about. There is MUCH more
to a Linux shell. Some of which we will cover shortly.

#### Applications
The next layer up go by many names:

Applications, Programs, commands, Binaries, Executables, software.

Whatever you call them applications are the bits which do the things you
actually want to do. They might be "ls" or "Microsoft Excel" or "Tetris"

So the applications do the things users want to do. And the kernel translates
those things into singals the hardware can understand. In the early days
applications talked to the kernel through the shell. These days they are able
to talk directly to the kernel and in some cases (usually only where video
performance is key) even talk to the hardware drivers directly.

## The Shell

As mentioned previously in Linux the shell is much more than just a textual
user interface.

A Linux shell is also a full fledged programming language. This allows you
to write small applications or "scripts" using the exact same syntax which  
you are already familiar with simply from using the OS.

In Linux required administrative tasks such as enabling the network interface
at startup or starting the web server which hosts your company's main
web site are all handled by "Shell scripts."

## Workstation Set Up

In business the computer you do your work on is often referred to as
your "workstation." In order to take full advantage of the rest of this book
you will need some common tools on your workstation. The below might not
be the best tools for the job or even my own personal favorites. They have
been chosen instead based on how easy they are to find directions for.
This course will be built around the following software.

  * A fancy text editor or basic IDE. We'll cover what those mean later but I might
    recommend [VSCode](https://code.visualstudio.com/download)
  * git - If you are on Windows download the official git https://git-scm.com/downloads
    and during the install say yes when it asks you to install gitbash.
  * A terminal emulator or command line interface. For early lessons gitbash
    will suffice but later we will want something better. I HAVE NOT YET DECIDED
    WHICH BETTER TOOL IS BETTER ENOUGH
    * If you are running MacOS "Terminal" is found in the "Utilities" subfolder of "Applications."
    * If you are on a Linux desktop you have a terminal emulator but who knows what it is called.
  * A web browser - It's hard to imagine an OS which doesn't ship with a browser these days.
    It could be Firefox, Chrome, Edge, Safari or a number of others.
  * There will probably be more later but that's it for now.
