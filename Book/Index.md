# Index


## [History](History.md)
  * Unix Wizards
  * The Rise of the Cloud

## What is *nix? - Short very high level we want to dive into useful as quickly as possible.    ye ol' circles diagram
  * What is a shell - Tiny bit about that and a 'more later.'

## [Navigation in Linux](Chapter_2.md)
  * overview of the file system (VERY high level.)
    * how it's a tree
    * how everything is technically a file and we'll cover the implications of that later.
  * cd, ls, cat
  * paths
    * implicit vs explicit
  * .files
  * permissions - Like not even permissions 101 like permissions 20. Just the very basics of the 3 sets of 3. Maybe not even covering the numeric values in detail. But definately covering the behavior diff of x on files and dirs

## The unix mindset and pipelining commands together
  * Do one thing do it right
  * stdin, out, err
  * tr, cut and sort

## The process tree
  * ps, kill
  * process parentage, orphaning - And something funny about orphan zombies.
  * backgrounding jobs
    * & and nohup, maybe disown though I've never used it.
  * A tiny bit about niceness.

## Permissions - A bit more in depth
  * Review
  * chown, chmod
  * Sticky bit

## The Shell
  * The shell environment - Variables, functions, aliases
  * Shell rc scripts
  * command line globbing
  * environment inheritance (or not)

## A bit more history - ?
  * Sys V args vs BSD args.

## Daemons
  * what it says on the tin

## Command history fun
  * Maybe not ALL the garbage from Hal's doc.

## Finding things
  * which
  * locate
  * find

## Slightly more advanced command line fu
  * ; and \
  * this && that || that
  * xargs
  * while true
  * for I in
  * lsof

## Vi Essentials
  * See my google doc.

## Networking in *nix
  * Networking overview
  * Ports
  * Binding to an interface and port ("listening on a port")
  * ping and tracert
  * Curl (not wget)
  * netcat
  * High level TCP/IP

## The init process in slightly greater detail - Rc directories vs systemd maybe?


## Modern Garbage - Maybe link these earlier in so they don't get skipped?
  * Containers (docker and otherwise) - Overview, history, some concerns when creating them.
  * The implications of containers and process adoption.
  * git
  * jq

## A bit more about docker - Specifically as it relates to this doc
  * Port binding and containers - I.E. why you have to
  * Why containers don't know their address or hostname
  * Why scratch containers work
  * container layers
  * container troubleshooting

## Installing and Configuring Software
  * Package managers
    
## Managing Linux Systems
  * Don't - I.E. No really, Don't. Why?
    1) Pets vs cattle
    1) Why you should just put your garbage into containers and have a tiny configuration script that just installs docker and runs your container.
    1) A tiny overview of config management stuff.
