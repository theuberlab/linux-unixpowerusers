### Redirecting Command Output

When UNIX was being created CPUs weren't very powerful. Memory was small  
and disks were so expensive and complex many computers didn't even have one.

To help minimize the amount of space the base operating system took up
as well as provide a more flexible user experience one of the core philosophies
the creators of UNIX adopted was "Do one thing and do it will."

This means instead of trying to write a single tool that does everything.
UNIX (and by extension Linux) commands tend to be light weight commands
which do one particular thing and offer a lot of flexibility on how to do it.


Cat is a great example simply because of it's name. The reason it's called  
`cat` is because it's short for 'concatenate'. The cat command was created  
to concatenate files together. It does this by taking a list of files as
arguments and then outputting all of them one after the other.

If we can each of the small files one

```
[ec2-user@ip-172-31-40-172 ~]$ cat aSmallFile.txt 
This is a file with some text in it.
Not much but a little.
```

at a

```
[ec2-user@ip-172-31-40-172 ~]$ cat anotherSmallFile.txt 
This is also a small file.
It has slightly more text.
Because it has four entire lines
instead of just three
[ec2-user@ip-172-31-40-172 ~]$ 
```

time

```
[ec2-user@ip-172-31-40-172 ~]$ cat yetAnotherSmallFile.txt 
This is an even smaller file.
[ec2-user@ip-172-31-40-172 ~]$ 
```

We can see their contents. Or we can conCATenate all three together
and view them as if they were a single file.

```
[ec2-user@ip-172-31-40-172 ~]$ cat aSmallFile.txt anotherSmallFile.txt yetAnotherSmallFile.txt 
This is a file with some text in it.
Not much but a little.
This is also a small file.
It has slightly more text.
Because it has four entire lines
instead of just three
This is an even smaller file.
[ec2-user@ip-172-31-40-172 ~]$ 
```

Which could be a lot more useful if we were able to save the results to a new file.

Enter command line redirection.

With it we can redirect the output of a command
into a new file.

The most common way to redirect output to a file is by using the simple redirect operator `>`
this `cat aSmallFile.txt anotherSmallFile.txt yetAnotherSmallFile.txt > aNewLessSmallFile.txt`:

```
[ec2-user@ip-172-31-40-172 ~]$ ls -l
total 12
-rw-rw-r-- 1 ec2-user ec2-user 109 Dec 24 21:00 anotherSmallFile.txt
-rw-rw-r-- 1 ec2-user ec2-user  60 Dec 24 18:54 aSmallFile.txt
drwxrwxr-x 2 ec2-user ec2-user  30 Dec 24 20:09 dir1
drwxrwxr-x 2 ec2-user ec2-user  55 Dec 24 19:05 dir2
-rw-rw-r-- 1 ec2-user ec2-user  30 Dec 24 21:00 yetAnotherSmallFile.txt
[ec2-user@ip-172-31-40-172 ~]$ cat aSmallFile.txt anotherSmallFile.txt yetAnotherSmallFile.txt > aNewLessSmallFile.txt
[ec2-user@ip-172-31-40-172 ~]$ ls -l
total 16
-rw-rw-r-- 1 ec2-user ec2-user 199 Dec 24 21:04 aNewLessSmallFile.txt
-rw-rw-r-- 1 ec2-user ec2-user 109 Dec 24 21:00 anotherSmallFile.txt
-rw-rw-r-- 1 ec2-user ec2-user  60 Dec 24 18:54 aSmallFile.txt
drwxrwxr-x 2 ec2-user ec2-user  30 Dec 24 20:09 dir1
drwxrwxr-x 2 ec2-user ec2-user  55 Dec 24 19:05 dir2
-rw-rw-r-- 1 ec2-user ec2-user  30 Dec 24 21:00 yetAnotherSmallFile.txt
[ec2-user@ip-172-31-40-172 ~]$ 
```



this is my text



There are two types of redirection.


As you can see head prints out the first few lines of a file and tail prints
out the last few. How many lines? The empty lines make the output a little
misleading so let's turn to another tool `wc` to find out.

`wc` stands for "Word Count" which isn't what we want. So we are going to
use the `-l` flag on wordcount to get the number of lines.


```
[ec2-user@ip-172-31-40-172 ~]$ wc -l dir1/aLargishTextFile
1543824 dir1/aLargishTextFile
[ec2-user@ip-172-31-40-172 ~]$ 
```

