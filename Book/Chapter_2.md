# The Filesystem
If you've used a computer for more than a few minutes you have at some point
had to navigate through folders full of files. In Linux these folders are
called directories and the collection of directories and files that make up
your computer is called the filesystem.

Early on the developers of Unix decided that they could simplify organization  
and interactions by treating everything like a file. And I mean everything  
Not just text files and binary files but on a UNIX descendent system somewhere  
in your filesystem there is a 'file' for your keyboard. There are files  
for your disk and monitor. We'll get in to the implications of this in more
detail later. For now just know that you will be able to find, and to a lesser
degree interact with, literally every component of your computer using the
tools presented in this chapter.

Unix filesystems are arranged in a tree structure. At the very base of this
tree is the directory `/` which is pronounced "root." Branching off of `/`
(which we refer to as "below") are a number of common directories such as  
`etc` (pronounced "et cee",) `bin`, and `var` as well as others.

A simplified view of a typical Linux system might look like this:

```
                      /
                      |
----------------------------------------------
|            |          |          |         |
bin/         dev/       etc/       var/      proc/
  |            |          |          |         |
  - ls         - sda      - passwwd  - log/    - meminfo
  - cat        - ttys1    - group    - run/    - filesystems
  - cd         - fd/      - init
```

As you may have surmised from their similarity to / names ending in `/`
represent directories. Thus the full path to the executable `ls` would be
`/bin/ls`. This is called an explicit path. An explicit path name is one
which begins at the root of the filesystem `/`

Explicit paths are very useful when you want to be... Explicit... when
referencing a particular file (Deleting or modifying the wrong file
can potentially result in disastrous consequences.) However it becomes  
cumbersome very quickly. On my laptop the path to the file you are currently  
reading is `/home/sporkboy/go/src/gitlab.com/theuberlab/linux-unixpowerusers/Book/Chapter_2.md`

To avoid all of this typing Linux supports another syntax called "Relative
paths." A relative path is one which is relative to your current working
directory or "cwd."

For example if my cwd was `/home/sporkboy/go/src/gitlab.com/theuberlab/linux-unixpowerusers/Book/`
the relative path to this file would be `Chapter_2.md`. If cwd was one directory
"up" at `/home/sporkboy/go/src/gitlab.com/theuberlab/linux-unixpowerusers/`
the relative path would be `Book/Chapter_2.md`.

Note: On a MacOS system these paths would be `/Users/sporkboy/go/src/gitlab.com/theuberlab/linux-unixpowerusers/Book/Chapter_2.md`

## Filesystem Navigation

You can determine your working directory with the command `pwd` (Print
Working Directory. To change your working directory you will use the command
cd (Change Directory.)

Lets try this out now.
Open a terminal
INSERT DIRECTIONS FOR MACOS AND WINDOWS WITH WSL

Once the terminal is open you should see something simiar to

```
sporkboy@tiktoc:~$ 
```

This is called your command prompt. If your's is different don't worry.
We will cover how to modify your prompt in later chapters.


At the command prompt type `pwd` and hit enter. You should see something
like

```
sporkboy@tiktoc:~$ pwd
/home/sporkboy
sporkboy@tiktoc:~$ 
```

Congratulations! You have just successfully executed (run) a Linux command.

In Linux parlance you have executed the command `pwd`. The command has 'returned'
it's results '/home/sporkboy' and 'exited' (quit) returning you to the
command prompt.

Did you notice how the value to the left of the '@' is the same as the last  
component of the directory path? When you first open a terminal or log in  
to a Linux system your 'cwd' will be the "home directory" for the user you  
are logged in as.

Note: We don't typically use terms like 'change cwd to.' Instead we say  
"Change Directories to" or simply "cd to."

Note: We also don't normally refer to "cwd" instead we would say we are  
"in the directory /home/sporkboy"

To change to a different directory we use the command `cd` (Change Directory.)
Let's move to the '/home' directory using it's explicit path. This is done
by typing the command, in this case `cd`, followed by a space and then the
path to the directory you wish to change to. Remembering that explicit paths
start with root that gives us `cd /home/`. Let's try this then use `pwd`
to confirm that it worked.

```
sporkboy@tiktoc:~$ pwd
/home/sporkboy
sporkboy@tiktoc:~$ cd /home/
sporkboy@tiktoc:/home$ pwd
/home/
sporkboy@tiktoc:/home$ 
```

If you haven't already modified your prompt it will _likely_ have changed.
This is simply a convention. Typically your prompt will include some
portion of the path to your 'cwd.'

From here let's change directories a couple more times. To 'bin/' and then
to '/var/log.' each time using `pwd` to confirm where we are.

```
sporkboy@tiktoc:/home$ pwd
/home/
sporkboy@tiktoc:/home$ cd /bin
sporkboy@tiktoc:/bin$ pwd
/bin/ 
sporkboy@tiktoc:/bin$ cd /var/log
sporkboy@tiktoc:/var/log$ pwd
/var/log/
sporkboy@tiktoc:/var/log$ 
```

Notice two things in the above. The first is that these two lines:
```
sporkboy@tiktoc:/home$ cd /bin
sporkboy@tiktoc:/bin$ pwd
```

Do not have any text between them. This is because the cd command does not
typically produce any output.

The second is this command
```
sporkboy@tiktoc:/bin$ cd /var/log  
```

Notice that I omitted the trailing slash '/'. Most (but not all) commands
which can act on directories will treat the trailing slash as optional.
However it is very important to remember. Some commands may act differently
if you execute them with vs without the trailing slash.

Before moving on let's disect that command slightly.

```
cd /var/log
```

Here `cd` is the command and '/var/log' is an "argument." Supplying an argument  
like this is called "Passing an argument." We have passed the argument "/var/log"
to the command `cd`. This tells `cd` which directory to change to.

So what happens if you don't pass any arguments? That depends entirely upon
the command. Let's see what happens in the case of cd.

```
sporkboy@tiktoc:/var/log$ pwd
/var/log/
sporkboy@tiktoc:/var/log$ cd
sporkboy@tiktoc:~$ pwd
/home/sporkboy
sporkboy@tiktoc:~$ 
```

Without any arguments `cd` will change 'cwd' to your home directory.

Now run `cd -`

```
sporkboy@tiktoc:~$ pwd
/home/sporkboy
sporkboy@tiktoc:~$ cd -
sporkboy@tiktoc:/var/log$ pwd
/var/log/
sporkboy@tiktoc:/var/log$ 
```

`cd` provides a shortcut '-' as a reference to your previous working directory.

### Summary
  * All the data on your system is collectively known as 'the filesystem'
  * Everything, even directories, in UNIX is a file.

### New Commands
  * `cd <directory>` - Change directory - To move from one directory to another.
  * `pwd` - Print Working Directory - To determine what directory you are currently in.
  * `cd -` - Changes to the last directory you were in.
  * `cd`- Without any arguments `cd` changes directories to your home directory.


## Interacting with Files and Directories

Now that's great if you know where you want to go. But often you'll need to
look to see what directories (and files) are available before changing directories.

To list the contents of a directory we use the command `ls` (list.) Without  
any arguments ls will list the contents of your working directory.

Assuming you are still in '/var/log' ('/var/log' is still your cwd)

```
sporkboy@tiktoc:/var/log$ pwd
/var/log/
sporkboy@tiktoc:/var/log$ ls
alternatives.log  apt  btmp  dist-upgrade  dpkg.log  journal  landscape  lastlog  unattended-upgrades  wtmp
sporkboy@tiktoc:/var/log$
```

Like `cd`, `ls` takes arguments, meaning we can issue commands such as:

```
sporkboy@tiktoc:/var/log$ ls /
bin  boot  dev  etc  home  init  lib  lib32  lib64  libx32  media  mnt  opt  proc  root  run  sbin  snap  srv  sys  tmp  usr  var
sporkboy@tiktoc:/var/log$
```


But how do we tell what is a file and what is a directory. Unlike the Windows and MacOS GUI filesystem browsers
there are no icons. `ls` can solve this in a couple of ways. All of which are activated through the
use of a special kind of argument called a 'flag.' Flags typically (more on this later) start with a dash '-'
or a double dash '--'

Flags for most built in Linux commands are a single character. let's pass the '-F' flag to ls. Let's list the contents
of root again this time adding '-F'.

```
sporkboy@tiktoc:/var/log$ ls -F /
bin@  boot/  dev/  etc/  home/  init*  lib@  lib32@  lib64@  libx32@  media/  mnt/  opt/  proc/  root/  run/  sbin@  snap/  srv/  sys/  tmp/  usr/  var/
sporkboy@tiktoc:/var/log$
```

Look at all those trailing slashes. Hopefully you're beginning to internalize that convention and recognize all of those as directories.
The other suffixes you will likely see are '*' (star) indicating that the file is executable, and '@' (at) indicating that the file is a
symbolic link. We'll cover links in more detail later. The short answer is that symbolic links are pointers to other files. Like Windows
'Shortcuts' and MacOS 'Aliases'. 

What if your output didn't have suffixes like the above? If it did not, it probably looked more like this:

```
sporkboy@tiktoc:/var/log$ ls -f /
.  ..  bin  boot  dev  etc  home  init  lib  lib32  lib64  libx32  media  mnt  opt  proc  root  run  sbin  snap  srv  sys  tmp  usr  var
```

Linux systems are case sensitive. This means that 'ls' is different from 'Ls,' 'lS,' and 'LS.' Only the correct case ('ls') will work.
This applies to command arguments as well as the commands themselves and the files they are acting on (remember that directories
are a special type of file.)

### Hiden Files

If you're paying attention you may have also noticed two new files in the output of that last command '.' and '..' To get a better
idea of what they are let's list root again but this time use both flags.

```
sporkboy@tiktoc:~$ ls -f -F /
./  ../  bin@  boot/  dev/  etc/  home/  init*  lib@  lib32@  lib64@  libx32@  media/  mnt/  opt/  proc/  root/  run/  sbin@  snap/  srv/  sys/  tmp/  usr/  var/
sporkboy@tiktoc:~$
```

As you can see those two files are directories. They are special directories applied for convenience. './' (dot slash) and '../' (dot dot slash)
will exist in every directory on the filesystem. './' is a reference to the directory itself while '../' is a reference to the
parent directory (the directory above the directory in question)

Take a look at the following commands:

```
sporkboy@tiktoc:/var/log$ ls
alternatives.log  apt  btmp  dist-upgrade  dpkg.log  journal  landscape  lastlog  unattended-upgrades  wtmp
sporkboy@tiktoc:/var/log$ ls ./
alternatives.log  apt  btmp  dist-upgrade  dpkg.log  journal  landscape  lastlog  unattended-upgrades  wtmp
sporkboy@tiktoc:/var/log$ ls ../
backups  cache  crash  lib  local  lock  log  mail  opt  run  snap  spool  tmp
sporkboy@tiktoc:/var/log$
```

Here we can see that `ls` is listing the contents of '/var/log'. `ls ./` produces the same results while `ls ../`
lists the _parent_ of '/var/log' ('/var') you can see log in the middle there between 'lock' and 'mail.'

These special directories can be used with virtually any command which can take a directory as an argument.

```
sporkboy@tiktoc:/var/log$ pwd
/var/log
sporkboy@tiktoc:/var/log$ cd ../
sporkboy@tiktoc:/var$ pwd
/var
sporkboy@tiktoc:/var$
```

But why didn't we see those directories earlier? This is because on Linux files who's names begin
with a dot are 'hidden' from normal output. This allows you keep long lived files along side shorter lived
files without having to scroll through potentially hundreds of files to find the one you are working on.
Applications frequently use this convention to keep their settings in your home directory. Let's
take a look at some.


cd to your home directory and run `ls -a` (the minus a flag is called 'all')

Remember that `cd` without any arguments takes you to your home directory.

```
sporkboy@tiktoc:/var$ pwd
/var
sporkboy@tiktoc:/var$ cd
sporkboy@tiktoc:~$ pwd
/home/sporkboy
sporkboy@tiktoc:~$ ls -aF
./  ../  .bash_history  .bash_logout  .bashrc  .lesshst  .motd_shown  .profile  .sudo_as_admin_successful  go@
sporkboy@tiktoc:~$ ls
go
sporkboy@tiktoc:~$
```

As you can see I have five hidden files in addition to the two special "dot directories" and the one normal
"go" directory. 


### Summary
  * './' and '../' - "Dot" and "Dot dot" (or "Dot slash" and "Dot dot slash") Are convenience directories which
  exist within all directories in the filesystem.
  * files beginning with a . are 'hidden' from normal output.

### New Commands and Arguments
  * `ls` - Lists the contents of a directory
  * `ls -a` - List ALL files in the directory, including hidden files.
  * `ls -F` - Appends a suffix to filenames indicating what type of file (directory, regular file, symbolic link, more) it is.






## Interacting With Regular Files


GOING TERSE HERE

Going to swich to a super terse style here just so that I can get the
outline done. I can flesh it out later.


### File Permissions
Traditional UNIX file permissions are pretty easy to understand. There are
three privledges which are granted to three groups of users.

The privledges are Read, Write and Execute, represented in text as r, w,
and x respectively.

The groupings of users are the individual user who owns the file, The group
who owns the file and lastly any and every user logged in to the system.  
Thi last grouping is referred to as "other."


Now Let's take another look and add the '-l' (long listing) flag.

```
sporkboy@tiktoc:~$ ls -fFl /
total 620
drwxr-xr-x  1 root root   4096 Dec 22 11:36 ./
drwxr-xr-x  1 root root   4096 Dec 22 11:36 ../
lrwxrwxrwx  1 root root      7 Aug  4 14:39 bin -> usr/bin/
drwxr-xr-x  1 root root   4096 Aug  4 14:47 boot/
drwxr-xr-x  1 root root   4096 Dec 22 12:33 dev/
drwxr-xr-x  1 root root   4096 Dec 22 11:38 etc/
drwxr-xr-x  1 root root   4096 Dec 22 11:35 home/
-rwxr-xr-x  1 root root 631968 Sep 12 23:57 init*
lrwxrwxrwx  1 root root      7 Aug  4 14:39 lib -> usr/lib/
lrwxrwxrwx  1 root root      9 Aug  4 14:39 lib32 -> usr/lib32/
lrwxrwxrwx  1 root root      9 Aug  4 14:39 lib64 -> usr/lib64/
lrwxrwxrwx  1 root root     10 Aug  4 14:39 libx32 -> usr/libx32/
drwxr-xr-x  1 root root   4096 Aug  4 14:39 media/
drwxr-xr-x  1 root root   4096 Dec 19 17:55 mnt/
drwxr-xr-x  1 root root   4096 Aug  4 14:39 opt/
dr-xr-xr-x 14 root root      0 Dec 19 17:59 proc/
drwx------  1 root root   4096 Dec 22 12:47 root/
drwxr-xr-x  1 root root   4096 Dec 22 11:31 run/
lrwxrwxrwx  1 root root      8 Aug  4 14:39 sbin -> usr/sbin/
drwxr-xr-x  1 root root   4096 Jul 10 06:59 snap/
drwxr-xr-x  1 root root   4096 Aug  4 14:39 srv/
dr-xr-xr-x 12 root root      0 Dec 19 17:59 sys/
drwxrwxrwt  1 root root   4096 Dec 22 11:31 tmp/
drwxr-xr-x  1 root root   4096 Aug  4 14:40 usr/
drwxr-xr-x  1 root root   4096 Aug  4 14:42 var/
sporkboy@tiktoc:~$
```

Note the flags `-fFl`. MOST flags can be stuck together like that.
A more detailed explaination will come shortly.

Here we see the same directories (the right hand column) but in a slightly
different format. The most immediately noticable is the presence of a lot
more data.

Here are the first five entries with a short description of the column values.
```
Permissions #Links Owner(User) Owner(Group) Size    MTime         Name
     |         |       |            |         |       |             |
drwxr-xr-x     1      root         root     4096 Dec 22 11:36   ./
drwxr-xr-x     1      root         root     4096 Dec 22 11:36   ../
lrwxrwxrwx     1      root         root        7 Aug  4 14:39   bin -> usr/bin/
drwxr-xr-x     1      root         root     4096 Aug  4 14:47   boot/
drwxr-xr-x     1      root         root     4096 Dec 22 12:33   dev/
```

Column descriptions:
  * Permissions - The permissions (and special) bits set on the file.
  * \#Links - The number of hard links to the file. Don't worry about this until later.
  * Owner(User) - The individual account which is the 'owner' of the file.
  * Owner(Group) - The group owner of the file.
  * Size - The size (typically in blocks by default) of the file.
  * MTime - When the file was last modified.
  * Name - The name of the file.




#### Summary

#### New Commands and Arguments
  * `-l` - `ls` flag for 'long'


### File Sizes and Types

TODO: Find a way to get thim into a specific directory. Git requires already
knowing the things I'm covering here.

Do an LS
```
[ec2-user@ip-172-31-40-172 ~]$ ls
aSmallFile.txt  dir1  dir2
[ec2-user@ip-172-31-40-172 ~]$ 
```

Add -l
```
[ec2-user@ip-172-31-40-172 ~]$ ls -l
total 4
-rw-rw-r-- 1 ec2-user ec2-user 60 Dec 24 18:54 aSmallFile.txt
drwxrwxr-x 2 ec2-user ec2-user 30 Dec 24 19:04 dir1
drwxrwxr-x 2 ec2-user ec2-user 55 Dec 24 19:05 dir2
[ec2-user@ip-172-31-40-172 ~]$ 
```

Remember we were ignoring the first character of the permissions column?
Stop ignoring it. Another way to tell if a file is a directory or not is
to look at the long listing of the directory (`ls -l`.) If it is a directory
that first character will typically be a 'd' like you see here.

So now that we know 'dir1' and 'dir2' are directories let's take a look in each of them.

```
[ec2-user@ip-172-31-40-172 ~]$ ls -l dir1
total 454256
-rw-rw-r-- 1 ec2-user ec2-user 465154352 Dec 24 19:02 aLargishTextFile
[ec2-user@ip-172-31-40-172 ~]$ 
```

Only one file in there, but wait, '465154352' blocks? Exactly how big *IS*
that file?

Let's add `-h` to our ls while keeping `-l`.

```
[ec2-user@ip-172-31-40-172 ~]$ ls -lh dir1
total 444M
-rw-rw-r-- 1 ec2-user ec2-user 444M Dec 24 19:02 aLargishTextFile
[ec2-user@ip-172-31-40-172 ~]$ 
```

Here we see '465154352' blocks has been translated into '444M' which means
this file is 444 Megabytes in size. Likewise you may see K (Kilobytes) G
(Gigabytes) or even larger.

`-h` stands for "Human Readable." and it translates the size column into
the largest size that makes sense. If we look at 'dir2' you'll see what I mean.

```
[ec2-user@ip-172-31-40-172 ~]$ ls -lh dir2
total 451M
-rw-rw-r-- 1 ec2-user ec2-user 450M Dec 24 19:04 aLargishBinaryFile
-rw-rw-r-- 1 ec2-user ec2-user 3.3K Dec 24 18:55 aMediumFile.txt
[ec2-user@ip-172-31-40-172 ~]$ 
```

Here we see another rather large file (aLargishBinaryFile at 450 Megabytes.)
along side a much smaller one at only 3.3 Kilobytes.


Let's take another look at the contents of our current working directory.

```
[ec2-user@ip-172-31-40-172 ~]$ ls -l
total 4
-rw-rw-r-- 1 ec2-user ec2-user 60 Dec 24 18:54 aSmallFile.txt
drwxrwxr-x 2 ec2-user ec2-user 30 Dec 24 19:04 dir1
drwxrwxr-x 2 ec2-user ec2-user 55 Dec 24 19:05 dir2
[ec2-user@ip-172-31-40-172 ~]$ 
```

Say you want to view the contents of 'aSmallFile.txt'. The simplest way is
with a command called `cat`. Let's "cat aSmallFile.txt".

```
[ec2-user@ip-172-31-40-172 ~]$ cat aSmallFile.txt 
This is a file with some text in it.
Not much but a little.
[ec2-user@ip-172-31-40-172 ~]$ 
```

That's it. 'aSmallFile.txt' contains the text:

```
This is a file with some text in it.
Not much but a little.
```

Cat has printed the contents of the file to your terminal and exited.

There was a medium sized file in one of those directories. Let's look at it.

```
[ec2-user@ip-172-31-40-172 ~]$ ls -l dir2/
total 460804
-rw-rw-r-- 1 ec2-user ec2-user 471859200 Dec 24 19:04 aLargishBinaryFile
-rw-rw-r-- 1 ec2-user ec2-user      3277 Dec 24 18:55 aMediumFile.txt
[ec2-user@ip-172-31-40-172 ~]$ ls -lh dir2/
total 451M
-rw-rw-r-- 1 ec2-user ec2-user 450M Dec 24 19:04 aLargishBinaryFile
-rw-rw-r-- 1 ec2-user ec2-user 3.3K Dec 24 18:55 aMediumFile.txt
[ec2-user@ip-172-31-40-172 ~]$ cat dir2/aMediumFile.txt 
This is a medium sized file with way more text

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet vestibulum dapibus. Nulla elementum eros quis est malesuada elementum. Praesent at est porta, fermentum tortor sit amet, tincidunt felis. Phasellus consectetur mollis ultricies. Proin congue commodo faucibus. Pellentesque blandit tristique velit. Cras volutpat bibendum ipsum, in mattis ante lobortis a. Quisque ac tincidunt ex. Maecenas ac pretium libero.

Phasellus pretium augue eu commodo sagittis. Vivamus magna arcu, tincidunt eu gravida et, viverra vel augue. Nam mattis congue bibendum. Praesent tincidunt tempor mauris sit amet fermentum. Mauris eget enim lacus. Fusce quis arcu iaculis, faucibus urna sit amet, molestie erat. Sed lacus mi, imperdiet posuere vulputate sed, aliquet nec metus. Sed lacinia luctus mi, et accumsan dui vestibulum vitae. Nam dictum, eros nec ultrices porta, augue massa lacinia sem, vel pharetra lectus dolor ac felis. Duis ante est, luctus ut eros in, dapibus fringilla odio. Morbi sodales odio eu ante consequat accumsan. Sed bibendum, erat ut ornare interdum, sapien leo consectetur est, vitae porta erat est sed quam. Duis id lorem vel felis pharetra finibus eget ut elit.

Quisque a neque enim. Nulla dignissim augue eu ligula scelerisque, id pellentesque diam blandit. Integer a ultricies elit. Donec in tortor quam. Maecenas tincidunt sapien sit amet tortor placerat, et fermentum leo pellentesque. Vestibulum elementum, velit vitae dapibus aliquet, sem lectus vestibulum mi, non finibus leo mauris in sem. In aliquam, nulla et convallis facilisis, turpis nibh facilisis lorem, blandit fringilla mauris massa vitae augue. Proin sem mi, vulputate congue augue id, scelerisque aliquet odio. In vestibulum elit nec vulputate pharetra. Ut ligula risus, posuere sit amet fringilla id, volutpat ut tortor. Vestibulum hendrerit tristique accumsan.

Nullam eget sapien id nisi mollis ultrices. Etiam justo sem, hendrerit non nunc nec, facilisis sodales metus. Praesent ac aliquam mi. Donec tristique diam at odio pellentesque laoreet. Etiam sit amet ex sit amet felis facilisis aliquet. Ut viverra tellus efficitur lectus pulvinar, non consectetur arcu dapibus. Quisque ut luctus ligula, in eleifend elit. Vestibulum volutpat sem sed tellus sollicitudin euismod. Maecenas eget faucibus nunc, ut sodales risus. Aenean orci justo, venenatis vitae laoreet aliquet, pharetra vel tellus.

Suspendisse auctor condimentum leo ac convallis. Cras vitae ullamcorper purus, non ultricies ipsum. Pellentesque pharetra rutrum augue, non aliquet lectus tincidunt vitae. Nulla sit amet odio euismod arcu pharetra consequat sed eu justo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer faucibus posuere ornare. Phasellus at dolor eget risus tincidunt consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin eleifend neque sit amet risus ultrices, nec consectetur diam pharetra. Duis mattis velit sed nulla eleifend, ac consequat ex auctor. In lacinia euismod purus non luctus. Cras ac urna at metus ornare elementum. Aliquam vehicula dictum pretium. Aliquam luctus, orci eget dignissim scelerisque, enim sapien sodales tortor, at lobortis purus metus id nisi. 
[ec2-user@ip-172-31-40-172 ~]$ 
```

Could you follow along that sequence of commands? First we performed a long
listing on 'dir2'.

```
[ec2-user@ip-172-31-40-172 ~]$ ls -l dir2/
```

Next we decided we wanted a simpler view of the size of the files in 'dir2'
and added `-h` to our ls command.

```
[ec2-user@ip-172-31-40-172 ~]$ ls -lh dir2/
```

And finally we decided 3.3 Kilobytes wasn't to bad so we catted 'aMediumFile.txt.'

```
[ec2-user@ip-172-31-40-172 ~]$ cat dir2/aMediumFile.txt
```


Try to cat aLargishBinaryFile

use `ctrl+c` to get out. If it kills your terminal do `ctrl+d` then log back in.

Now let's see what type of file this is. For this we'll use the command `file`

```
[ec2-user@ip-172-31-40-172 ~]$ cat dir2/aLargishBinaryFile 
^C
[ec2-user@ip-172-31-40-172 ~]$ file !$
file dir2/aLargishBinaryFile
dir2/aLargishBinaryFile: data
```

Back in 'dir1' there was another 'Largish' file. Let's see what type of file
that one is.

```
[ec2-user@ip-172-31-40-172 ~]$ file dir1/aLargishTextFile 
dir1/aLargishTextFile: ASCII text, with very long lines
[ec2-user@ip-172-31-40-172 ~]$ 
```

ASCII text, that should be safe. Let's cat that one...

But before you do. Remember `ctrl+c` to cancel the currently running command.

```
[ec2-user@ip-172-31-40-172 ~]$ head dir1/aLargishTextFile 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt erat at lacus porttitor interdum. Suspendisse est sem, semper et mauris quis, pretium varius enim. In sagittis posuere sollicitudin. Ut ante metus, luctus nec enim et, auctor suscipit enim. Vivamus non sagittis arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec porta arcu id erat porta pulvinar. Sed sit amet felis elementum, feugiat dui quis, sodales ipsum.

Sed tempor commodo orci id pulvinar. Suspendisse potenti. Morbi vehicula nibh sit amet ipsum condimentum, sed ultricies lacus iaculis. In in libero sit amet risus consequat pellentesque. Quisque cursus venenatis dui a hendrerit. Aenean ultricies malesuada magna at pellentesque. Aenean a blandit lorem. Vivamus commodo tortor nulla, eget scelerisque dolor consectetur a. Vestibulum non hendrerit lectus, in dignissim purus. Maecenas sed erat justo. Donec cursus non justo commodo rhoncus. Nullam purus lectus, placerat eget blandit non, posuere quis erat. Vivamus ut lectus molestie, accumsan enim ut, congue risus. Aliquam at iaculis sapien. Maecenas feugiat diam sed aliquet sollicitudin. Cras lobortis sodales egestas.

Nullam interdum bibendum ante, ac accumsan sem venenatis vel. Phasellus auctor arcu vel maximus interdum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vestibulum diam et ex sodales, vel tempor neque facilisis. Donec bibendum finibus velit vel posuere. Praesent sed metus massa. In felis lorem, molestie at lectus non, fringilla lacinia turpis. Pellentesque at enim eu mauris sodales sollicitudin. Maecenas sed accumsan justo. Nulla sem risus, luctus sed tellus eget, ultrices facilisis dui.

Proin ut interdum neque. Sed sodales, turpis a vehicula hendrerit, massa nibh sollicitudin nisi, sit amet bibendum elit erat id magna. Ut id risus a tortor convallis consectetur. Aenean metus metus, luctus a dui a, sagittis ornare nisl. Mauris vel leo sagittis, maximus lacus vitae, dapibus lectus. Vivamus dapibus purus ut ex ornare bibendum. Praesent nec nunc sit amet mi auctor pretium. Curabitur eget augue commodo justo ultricies finibus. Cras eu neque ac mi egestas aliquet ut ut nunc. Vivamus finibus lorem a diam posuere semper. Nunc venenatis ultrices vulputate. Pellentesque tincidunt tempor auctor. Nullam et velit in velit tincidunt efficitur non vitae tellus.

Sed dolor est, dictum ut nisl at, lobortis vulputate diam. Nulla facilisi. Suspendisse mattis urna a eleifend venenatis. Sed mollis vestibulum interdum. Ut volutpat velit eu mollis efficitur. Mauris mauris magna, euismod pellentesque purus non, viverra faucibus massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas finibus consectetur dapibus. Maecenas rhoncus, nunc vel varius semper, diam mauris semper urna, mollis vehicula sapien dolor ac magna. In vel efficitur enim.

Suspendisse sagittis dolor eros, vitae luctus turpis tincidunt non. In congue scelerisque scelerisque. Integer fringilla sed arcu quis tempor. Phasellus eu vulputate lorem, non lobortis nibh. Sed sit amet vulputate diam, et ornare lectus. Maecenas sollicitudin vehicula volutpat. Sed non scelerisque elit, eu pretium metus. Suspendisse eget ullamcorper orci. Sed vulputate, diam at rhoncus vulputate, ipsum tortor volutpat justo, vitae varius magna ligula at metus. Morbi commodo, urna a vestibulum suscipit, arcu quam ultricies metus, sit amet vehicula massa tortor vitae nisi. Praesent quis est vel nulla porttitor sodales sit amet ut nunc.

Nulla viverra, odio non bibendum conse^C
```

That '^C' is where I hit 'ctrl+c' to cancel the output. Did you sit through the whole thing.

Turns out that 'aLargishTextFile' is over 1.5 *million* lines long. So perhaps
cat isn't the best way to view a file. What if you just want to make sure it's
the correct file? The simplest way is likely to be by looking at the beginning
or the end of the file.

To do this you _could_ cat the file and immediately hit 'ctrl+c' but that
won't be reliable. And it won't show you the end. Wouldn't it be easier
if you could just view the beginning or end of a file. Enter `head` and `tail`.

```
[ec2-user@ip-172-31-40-172 ~]$ head dir1/aLargishTextFile 
This is the very beginning of aLargishTextFile

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt erat at lacus porttitor interdum. Suspendisse est sem, semper et mauris quis, pretium varius enim. In sagittis posuere sollicitudin. Ut ante metus, luctus nec enim et, auctor suscipit enim. Vivamus non sagittis arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec porta arcu id erat porta pulvinar. Sed sit amet felis elementum, feugiat dui quis, sodales ipsum.

Sed tempor commodo orci id pulvinar. Suspendisse potenti. Morbi vehicula nibh sit amet ipsum condimentum, sed ultricies lacus iaculis. In in libero sit amet risus consequat pellentesque. Quisque cursus venenatis dui a hendrerit. Aenean ultricies malesuada magna at pellentesque. Aenean a blandit lorem. Vivamus commodo tortor nulla, eget scelerisque dolor consectetur a. Vestibulum non hendrerit lectus, in dignissim purus. Maecenas sed erat justo. Donec cursus non justo commodo rhoncus. Nullam purus lectus, placerat eget blandit non, posuere quis erat. Vivamus ut lectus molestie, accumsan enim ut, congue risus. Aliquam at iaculis sapien. Maecenas feugiat diam sed aliquet sollicitudin. Cras lobortis sodales egestas.

Nullam interdum bibendum ante, ac accumsan sem venenatis vel. Phasellus auctor arcu vel maximus interdum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vestibulum diam et ex sodales, vel tempor neque facilisis. Donec bibendum finibus velit vel posuere. Praesent sed metus massa. In felis lorem, molestie at lectus non, fringilla lacinia turpis. Pellentesque at enim eu mauris sodales sollicitudin. Maecenas sed accumsan justo. Nulla sem risus, luctus sed tellus eget, ultrices facilisis dui.

Proin ut interdum neque. Sed sodales, turpis a vehicula hendrerit, massa nibh sollicitudin nisi, sit amet bibendum elit erat id magna. Ut id risus a tortor convallis consectetur. Aenean metus metus, luctus a dui a, sagittis ornare nisl. Mauris vel leo sagittis, maximus lacus vitae, dapibus lectus. Vivamus dapibus purus ut ex ornare bibendum. Praesent nec nunc sit amet mi auctor pretium. Curabitur eget augue commodo justo ultricies finibus. Cras eu neque ac mi egestas aliquet ut ut nunc. Vivamus finibus lorem a diam posuere semper. Nunc venenatis ultrices vulputate. Pellentesque tincidunt tempor auctor. Nullam et velit in velit tincidunt efficitur non vitae tellus.

[ec2-user@ip-172-31-40-172 ~]$ 
[ec2-user@ip-172-31-40-172 ~]$ tail dir1/aLargishTextFile 
Etiam accumsan sapien vitae justo consectetur, ac euismod mi semper. Suspendisse auctor ut diam ac commodo. Vivamus aliquet, mi eu dapibus posuere, odio quam dictum erat, ac ultrices justo arcu vitae neque. In et vulputate metus, vel dignissim massa. Nam eget convallis velit. Maecenas id suscipit leo, vitae scelerisque dolor. Donec eget posuere tortor. Proin dolor erat, interdum sit amet erat vitae, interdum volutpat ex. Sed quis erat commodo, elementum augue non, mattis ante. Suspendisse potenti. Fusce sit amet laoreet mi. Integer tincidunt pulvinar est, vitae convallis nunc semper a. Nulla finibus feugiat sem in dignissim. Interdum et malesuada fames ac ante ipsum primis in faucibus.

Quisque ornare a felis ut congue. Vivamus quis cursus metus, ac laoreet ante. Donec pretium facilisis nibh sed euismod. Phasellus purus erat, porttitor in semper consequat, lacinia eget mi. Maecenas interdum dapibus massa, non tincidunt enim. Phasellus varius et velit vel dignissim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis elementum orci sit amet quam commodo sagittis. Donec sit amet mi vel neque lacinia egestas a ac ex. Nam auctor nunc ut turpis facilisis suscipit. Sed auctor ultricies ex. Morbi sed porta tortor, nec malesuada tortor.

Ut volutpat sapien lorem, vitae ornare odio bibendum nec. Integer scelerisque nibh mi, sed tincidunt diam varius id. Mauris in lacinia turpis. Nam vitae felis turpis. Nunc urna erat, iaculis ac sapien id, hendrerit gravida est. Aenean sodales est in vestibulum facilisis. Suspendisse nec odio consequat, feugiat massa at, tempor dui. Vivamus volutpat ipsum vitae imperdiet viverra. Maecenas dictum dignissim risus, non hendrerit nulla vulputate quis. Integer dictum odio et erat iaculis tempus.

Ut turpis tellus, tristique a leo eget, feugiat volutpat nibh. Fusce vitae nunc lacus. Pellentesque accumsan purus erat, accumsan accumsan purus consequat eu. Vivamus in quam vel erat maximus vestibulum maximus id lorem. In eget aliquet enim. Integer ut erat nec urna scelerisque semper nec ultricies est. Sed eu dapibus arcu, sed lacinia augue. In eleifend dictum dictum. Curabitur at venenatis lorem. Vivamus at tincidunt erat. Pellentesque massa ex, dignissim ut est a, mattis commodo ipsum. Nam commodo erat id pharetra hendrerit. Integer quis mi nibh. Fusce sit amet tellus sed ligula fermentum bibendum.

Aenean enim orci, mattis sit amet placerat ut, aliquam egestas quam. Etiam suscipit efficitur sapien, et condimentum diam condimentum vel. Fusce sollicitudin consectetur risus, at lacinia neque lobortis sit amet. Etiam sit amet mi blandit, porttitor odio vel, suscipit felis. In vel lobortis lorem. Aenean venenatis fe
And this is the very end of aLargishTextFile
[ec2-user@ip-172-31-40-172 ~]$ 
```

#### Summary

#### New Commands and Arguments
  * head - View the beginning of a file
  * tail - View the end of a file
