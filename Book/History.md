# History


## What is Unix

To understand [Linux](https://en.wikipedia.org/wiki/Linux) we are going to (very briefly)
dive into it's history.

In the late sixties/early seventies computers were very large and cumbersom.
You interacted with them primarily by loading programs into them from punch
cards or by using a device called a "[Computer Terminal.](https://en.wikipedia.org/wiki/Computer_terminal)"  
A [computer terminal](https://en.wikipedia.org/wiki/Computer_terminal),
also called a "dumb terminal" or simply a "terminal" has a screen and a keyboard
(but no mouse) like you're used to today but had no actual storage(hard drive)
or intelligence (CPU) of it's own. In fact early versions didn't even have
a screen. Instead they were a printer and a keyboard. These machines were
[called "Teleprinters" or "Teletypes"](https://en.wikipedia.org/wiki/Teleprinter)
and you will still see some fingerprints of them later when we discuss
TTYs and Terminal settings.

A handful of very smart people who were frustrated with operating system
progress (A project called Multics) and wanted a less cumbersom OS upon
which to continue their other research (and in no small part to play
one of the worlds first computer games [Space Travel](https://en.wikipedia.org/wiki/Space_Travel_(video_game))
discovered an old un-used computer made by [DEC](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation)  
called a [PDP-7](https://en.wikipedia.org/wiki/PDP-7).

Due to Multics' bulkiness and various license restrictions they and ended  
up created an their own operating system. This OS [was called Unix.](https://en.wikipedia.org/wiki/Unix)

Unix ended up becoming the backbone of a US government funded research project
called [ARPANET](https://en.wikipedia.org/wiki/ARPANET). ARPANET was intended to
create a distributed, self healing network that could handle major interuptions
such as a Russian nuclear missle destroying NORAD. In the 80's commercial
interest in ARPANET resulted in the project being broken up into two new projects.
Milnet which was a defense only network. And a little project you may know
of as "The Internet."

By the late eighties the capabilities of Unix were highly regarded. So
much so that various vendors were offering competing versions of Unix.  
One of the results of this competition ([known as "The Unix Wars"](https://en.wikipedia.org/wiki/Unix_wars))
was that all versions of Unix were proprietary.

In order to combat these proprietary licenses which he saw as stifling the
creativity of the Unix community [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman)
created [The GNU Project](https://en.wikipedia.org/wiki/GNU_Project) The
GNU Project resulted in a full suite of Unix like command line tools but
no actual operating system. In the late 80s there had been some work to
make Unix available on PC grade hardware (see [MINIX](https://en.wikipedia.org/wiki/MINIX))
these were also non-free and had licenses preventing developers from contributing.

So in 1991, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
wrote what became the Linux Kernel and shortly thereafter the combination
of Linus' kernel and tooling from the GNU Project became [the Linux Operating system.](https://en.wikipedia.org/wiki/Linux)

A key component of both the Linux and GNU Projects is that they are complete
rewrites of the projects they are based on. They do not include nor were
they derived from any actual source code found in the original projects.  
This allowed them both to adopt an open licensing model enabling anyone to  
modify (and by extension contribute) to their repective code bases.

## Unix "Wizards"

Back in some of the earlier days of computing, back before there were good computer games,
before this silky thing called "the internet" (which is onviously just a fad) became a thing,
there were people we would call "Unix Power Users." These were often professors or research assistants at universities or Data Analysts at large companies.
They were folks like you or I who had a job to do. And to do their job they had to do something that
was still fairly novel at the time. They had to use a piece of software to do their job.

These days it's hard to imagine a job that doesn't require using some piece of software.
You could literally be digging a ditch at the side of the road and scheduling your work probably
involved an app or email. But at some point after the steam engine was invented and before Facebook knew everything about you
there was a time when using a computer was actually uncommon.

I'm not talking about punch cards, though some of these folks may have interacted with the system through a telatype.
During this era there really wasn't such a thing as "Off the shelf" software. If you were an accountant using a Unix system
it was probably because the organization you worked for had written their own software for performing the tracking and
analysis you were trying to perform.

Part if the reason for this was that these computers kind of sucked. There is probably more compute power
in a fitbit than the computers used to first put Human beings on the moon. The other reason is that they were enormous.
Due to the low power, high footprint and high cost they tended to be used for some rather specific purposes and
very little thought was given to making them easy to use.

Then the "Computer Revolution" happened and the "PC" computer gave us an interface that was much easier for the average
person and cheap enough that it became reasonable for a company to purchase a separate machine for every employee. People
in these same roles were now able to do their work with pretty spreadsheets or one of the myriad pieces of commercially
available software written to handle exactly the kind of task they were performing.

However, if you needed to do something at a very large scale the PC just couldn't cut it. So when "The Internet" happened
and companies found themselves wanting to write software which would serve data to millions of concurrent users they tended
towards Unix servers. Since end users didn't interact with these machines directly the only people who needed to understand
Unix were the actual Systems Administrators keeping the machines running. It was a very specific, highly desired and
barely understood skillset. The distribution or "flavor" of Unix was directly tied to the hardware which was expensive.

A handful of people began experimenting with porting Unix operating systems to this low cost easily accessable PC hardware.
But since the major Unix distributions were all proprietary it mostly involved writing all the tools and components from
scratch. Eventually several of these projects coalesced into a project called Linux. It was popular amongst hobyists and
experimenters but it was young and ran on machines that weren't very powerful.


THIS IS WRONG
The internet didn't really start to spread like fire until Linux occurred. Work on this bit more.


But the internet was a spark in kindling. It had the potential to revolutionize how we use computers. But to do so it was
going to have to expand beyond it's DOD research project roots. Suddenly this new proto operating system by some strange  
guy out of Finland had much more potential.

But the internet was spreading like fire. More and more businesses wanted the power of a Unix system without the pricetag.
Linux was almost as great as Unix but not all of your favorite tools existed there. You could probably compile the thing
you wanted by hand but the compilers of the era were still struggling with the massive array of different hardware so
your odds of success weren't great. As a result, Linux provided a great place to learn and often a good place to experiment
and develop new software ideas .but if you were a business and serious about stability you almost invariably took the
leap up to more powerful Unix systems provided by companies like Sun, SGI, IBM and HP.

Then two things happened at around the same time. PC hardware became dramatically more powerful (largely due to a desire
for better graphics in PC games) and Linux (arguably the entire open source movement) "grew up." Commercial distributions
of Linux started gaining visibility (and market share.) And as the standard bearers of the open source movement instead
of replacing the open source components of their distributions with entirely proprietary versions these companies
continued to include open source software and contribute back to these projects. Now some of these weekend experimenters
were able to work on these projects as their day jobs further increasing the viability of Linux as a production grade
offering.



## The Rise of "The Cloud"

