# Labs

In addition to the main documentation there should be a small set of labs.

I'd like to produce docker containers for these so that folks can run them
in a controlled environment where all the commands have the same names
and arguments as the ones we describe in the main text.

This isn't a full college course we don't have to go nuts with the labs.
