# Style Guide

Some notes on style when contributing to this work.


## Key Guidelines
  * Use a friendly conversational style
  * Get to hands on quickly
  * Provide real-world examples and background


## Chapters
Chapters cover a number of related technical topics devided into sections.

Chapters:
  * MAY include examples before diving into specific topics
  * MAY include a 'background' section which provides some history but which can
    be skipped by the reader without effecting their understanding of the chapters technical topics.

### Sections
Sections should start with a short overview of what will be covered which
is limited to only a couple of paragraphs before jumping into real world
hands on examples.

These first examples should be very simple. Just enough to give a taste
of what will be covered in the chapter.  
These examples:
  * SHOULD be no more than a single line
  * MUST all be runable.
  * SHOULD contain only a single step

Multi-step examples and examples which involve deliberately incorrect
steps (I.E. to teach users how to troubleshoot) will be included at the end
of the topic section.

Nex the section should move to a deeper dive on the topic to give readers an understanding
of what we are teaching them.

Finally it should wrap up with a more in-depth lab to solidify what they have
just been taught

