# Linux/Unix for Power Users

Tips and tricks for simplifying the lives of users of Linux and Unix operating systems.

## Who This is For
This book and (eventually) accompanying labs are directed at folks who  
interact with Linux or Unix systems during their daily lives but  
who have no need to become experts at managing said systems.

That includes:
  * Developers deploying to the cloud
  * Anyone trying to move workloads to containers
  * Nerds who want to switch to linux on their primary computer
  * Developers who would like to make requests to operational teams (systems admins, network admines, etc.) without being treated like they are morons.
  * Engineers keen on avoiding some obvious security mistakes
  * Basically anyone who has (or wants) to interact with a linux (or linux style) command line but feels that it "slows them down."

## What we will cover
  * Getting around on the command line
  * Finding the things you need
  * Extracting information from logs (and other files.)
  * WTH is all this docker garbage (and why it's not garbage)

## What we will not cover
  * Installing, tuning or optimizing Linux
  * Choosing a Linux distribution
  * Much of anyting under /proc or /sys
  * Anything significant about the differnce between a Linux OS and an actual Unix OS. For the purposes of this book MacOS is equivalent to Linux
  * Managing Linux systems in a production environment
  * Super "Kung-fu" skills like fancy sed and awk skills or shell scripts that involve more than 10 or 20 lines.

## [Take Me to the Knowledge Professor!](Book/Index.md)
[Start with the index here.](Book/Index.md)

## About
